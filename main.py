''' FastAPI '''

import re

import arrow
from fastapi import FastAPI, HTTPException, Request
from fastapi.templating import Jinja2Templates
from pymongo import AsyncMongoClient
from starlette.responses import RedirectResponse

app = FastAPI()
templates = Jinja2Templates(directory='.',
                            trim_blocks=True,
                            lstrip_blocks=True,
                            keep_trailing_newline=True)
db = AsyncMongoClient('mongodb://db').db


def translate(request: Request):
    ''' Detect client language and translate page labels '''

    translations = {
        'en': {
            'lang': 'en',
            'selected_en': ' selected',
            'not_found': 'Not found',
            'home': 'Home',
            'world': 'World',
            'about': 'About',
        },
        'el': {
            'lang': 'el',
            'selected_el': ' selected',
            'not_found': 'Δεν βρέθηκε',
            'home': 'Αρχική',
            'world': 'Κόσμος',
            'about': 'Πληροφορίες Ιστότοπου',
        }
    }

    language = 'en'
    translations[language]['parameter'] = ''

    if 'lang' in request.query_params and request.query_params[
            'lang'] in translations:
        language = request.query_params['lang']
        translations[language]['parameter'] = f'?lang={language}'
    elif 'accept-language' in request.headers:
        for lang_country in parse_accept_language(
                request.headers['accept-language']):
            lang = lang_country.split('-')[0]
            if lang in translations:
                language = lang
                translations[language]['parameter'] = ''
                break

    return translations[language]


async def guess_country(request: Request):
    ''' Detect client's country '''

    if 'accept-language' in request.headers:
        print(f"Accept-Language: {request.headers['accept-language']}")
        for lang_country in parse_accept_language(
                request.headers['accept-language']):
            lang_country_split = lang_country.split('-')
            if len(lang_country_split) < 2:
                continue
            country_code = lang_country_split[1]
            country = await country_route(country_code)
            if country:
                return country

    if 'x-country' in request.headers:
        print(f"X-Country: {request.headers['x-country']}")
        country = await country_route(request.headers['x-country'])
        if country:
            return country

    return None


def parse_accept_language(accept_language):
    ''' Parse browser's language header '''
    languages = accept_language.split(',')
    locale_q_pairs = []

    for language in languages:
        if len(language.split(';')) == 1:
            # no q => q = 1
            locale_q_pairs.append((language.strip(), 1))
        else:
            locale = language.split(';')[0].strip()
            try:
                q_weight = language.split(';')[1].split('=')[1]
                q_weight = float(q_weight)
            except (IndexError, ValueError):
                q_weight = 1
            locale_q_pairs.append((locale, q_weight))

    locale_q_pairs.sort(key=lambda x: x[1], reverse=True)
    return [locale_q[0] for locale_q in locale_q_pairs]


async def country_route(country_code: str):
    ''' Check if there're any news for given country '''
    document = await db['news'].find_one({'country': country_code})
    if document:
        return document['_id']
    return None


@app.get('/')
async def homepage(request: Request):
    ''' Home page redirection '''
    country = await guess_country(request)
    if country:
        return RedirectResponse(
            url=f"/{country}{translate(request)['parameter']}")
    return RedirectResponse(url=f"/world{translate(request)['parameter']}")


@app.get('/status')
async def get_status():
    ''' Returns 200 if client can reach database '''
    await db['status'].update_one(filter={'_id': 'OK'},
                                  update={'$set': {
                                      '_id': 'OK'
                                  }},
                                  upsert=True)
    return 'Back-end services operational'


@app.get('/about')
async def get_about(request: Request):
    ''' About page '''
    return templates.TemplateResponse(
        request, 'template.html', {
            'title': translate(request)['about'],
            'about': True,
            'translation': translate(request)
        })


@app.get('/world')
async def get_locations(request: Request):
    ''' Lists the available locations '''
    locations = []
    async for document in db['news'].find({'_id': {'$ne': 'hacker-news'}}).sort('name'):
        locations.append({
            'link': f"/{document['_id']}{translate(request)['parameter']}",
            'title': document['name'],
            'direction': document['direction'],
            'image': document['news'][0]['image'],
            'first_trend': re.search(r'<i>\W?(.*?)\W*</i>', document['news'][0]['title']).group(1)
        })
    return templates.TemplateResponse(
        request, 'template.html', {
            'title': translate(request)['world'],
            'posts': locations,
            'translation': translate(request)
        })


@app.get('/{location}')
async def get_news(request: Request, location: str):
    ''' A front-end for news '''
    document = await db['news'].find_one({'_id': location})
    if not document:
        raise HTTPException(404)
    for post in document['news']:
        lang = translate(request)['lang']
        post['date'] = arrow.get(post['date']).humanize(locale=lang)
    if 'direction' in document:
        direction = document['direction']
    else:
        direction = None
    return templates.TemplateResponse(
        request, 'template.html', {
            'title': document['name'],
            'direction': direction,
            'posts': document['news'],
            'translation': translate(request)
        })


@app.exception_handler(404)
async def custom_404_handler(request: Request, _):
    ''' 404 page '''
    return templates.TemplateResponse(
        request, 'template.html', {
            'title': f"404 {translate(request)['not_found']}",
            'error_404': True,
            'translation': translate(request)
        }, 404)
