FROM python:3.13.2
WORKDIR /app
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt
COPY main.py template.html /app
CMD ["fastapi", "run", "main.py", "--proxy-headers", "--workers", "8"]
